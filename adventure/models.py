import math
from pickle import FALSE
import re
from datetime import date
from tracemalloc import start

from django.core.exceptions import ValidationError
from django.db import models

# Create your models here.


class VehicleType(models.Model):
    name = models.CharField(max_length=32)
    max_capacity = models.PositiveIntegerField()

    def __str__(self) -> str:
        return self.name


class Vehicle(models.Model):
    name = models.CharField(max_length=32)
    passengers = models.PositiveIntegerField()
    vehicle_type = models.ForeignKey(VehicleType, null=True, on_delete=models.SET_NULL)
    number_plate = models.CharField(max_length=10)
    fuel_efficiency = models.DecimalField(max_digits=6, decimal_places=2) # in km/L
    fuel_tank_size = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self) -> str:
        return self.name

    def can_start(self) -> bool:
        return self.vehicle_type.max_capacity >= self.passengers
    def validate_number_plate(self):
        # Se quitan los espacios y guiones
        self.number_plate = self.number_plate.replace("-", "")
        self.number_plate = self.number_plate.replace(" ", "")
        # Se ve que la patente solo tenga 6 caracteres
        if len(self.number_plate)==6 and self.number_plate[0:2].isdigit() and self.number_plate[2:6].isalpha():
            return True
        else:
            return False
    def get_vehiculos():
        vehiculos=[]
        x=Vehicle.objects.all()
        for i in x:
            vehiculos.append(i)
        if(len(vehiculos)==0):
            return 0
        else:
            return vehiculos
    def get_vehiculos_license_plate():
        vehiculos=[]
        x=Vehicle.objects.all()
        for i in x:
            vehiculos.append(i.number_plate)
        if(len(vehiculos)==0):
            return 0
        else:
            return vehiculos

        
    
        
    def get_distribution(self) -> list:
        # Limitation: Fixed to 2 seats for row, this is not scalable but it's fast
        partial_row, fullrows = math.modf(self.passengers/2) # separate full rows from partials
        seat_distribution = [[True, True] for i in range(int(fullrows))]

        if partial_row > 0:
            seat_distribution.append([True,False])

        return seat_distribution

class Journey(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.PROTECT)
    start = models.DateField()
    end = models.DateField(null=True, blank=True)


    def agregar_fin_por_id_v(viajeId,viajeEnd):
        x=Journey.objects.filter(id=viajeId).update(end=viajeEnd)
        return x
        

    def __str__(self) -> str:
        return f"{self.vehicle.name} ({self.start} - {self.end})"
    
    def is_finished(self):
        return (self.end != None and self.end <= date.today())

class ServiceArea(models.Model):
    kilometer = models.IntegerField()
    gas_price = models.PositiveIntegerField()
    left_station = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True,related_name='left_stationtwo')
    right_station = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True,related_name='right_stationtwo')

    def validate_left(self):
        if self.left_station == None:
            return True

        if self.kilometer <= self.left_station.kilometer:
            raise ValidationError({'left_station': 'Stations to the left should be placed before the station.'})

    def validate_right(self):
        if self.right_station == None:
            return True
    
        if self.kilometer >= self.right_station.kilometer:
            raise ValidationError({'right_station': 'Stations to the right should be placed after the station.'})

    def get_service_area():
        areaservicio=[]
        x=ServiceArea.objects.all()
        for i in x:
            areaservicio.append(i)
        if(len(areaservicio)==0):
            return 0
        else:
            return areaservicio

    def get_service_area_kilometer():
        kilometros=[]
        x=ServiceArea.objects.all()
        for i in x:
            kilometros.append(i.kilometer)
        if(len(kilometros)==0):
            return 0
        else:
            return kilometros

    def clean(self, *args, **kwargs):
        self.validate_left()
        self.validate_right()
        super().clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)