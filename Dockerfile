# syntax=docker/dockerfile:1
FROM python:3
ENV PYTHONUNBUFFERED=1
RUN mkdir /code
WORKDIR /code
RUN apt-get clean && apt-get update && apt-get install -y locales
RUN locale-gen es_ES.UTF-8
RUN pip install --upgrade pip
RUN export LC_ALL="es_ES.UTF-8"
RUN export LC_CTYPE="es_ES.UTF-8"
RUN dpkg-reconfigure locales
COPY requirements.txt /code/
RUN python -m pip install -r requirements.txt

COPY . /code/